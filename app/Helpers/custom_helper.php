<?php

if(!function_exists('clean_html')) {
    function clean_html($value) {
        return strip_tags($value);
    }
}

if(!function_exists('affiliate_id')) {
    function affiliate_id($length = 10) {
        $characters       = '0123456789';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $existings_id = \App\Models\User::get_affiliates_id();
        if(count($existings_id)) {
            if(in_array($randomString, $existings_id)) {
                affiliate_id();
            }
        }
        return $randomString;
    }
}

if(!function_exists('my_affiliate_id')) {
    function my_affiliate_id() {
        return (Illuminate\Support\Facades\Auth::check()) ? Auth::user()->affiliate_id : null;
    }
}

if(!function_exists('this_route')) {
    function this_route() {
        return Illuminate\Support\Facades\Request::route()->getName();
    }
}
