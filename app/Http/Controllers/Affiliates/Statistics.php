<?php

namespace App\Http\Controllers\Affiliates;

use App\Http\Controllers\Controller;
use App\Models\Hits;
use App\Models\User;
use Illuminate\Http\Request;

class Statistics extends Controller {

    public function hits(Request $request, $affiliate, $client) {

        if(User::affiliate_not_exists($affiliate)) {
            abort(404);
        }

        $affiliate = User::by_affiliate_id($affiliate);

        if(User::client_not_exists($affiliate->id, $client)) {
            abort(404);
        }

        Hits::register($affiliate->id, $client);

        return 'OK';
    }

}
