<?php

namespace App\Http\Livewire\Affiliate;

use Livewire\Component;

class Dashboard extends Component {

    public $title   = 'Dashboard';
    public $section = 'dashboard';

    public function render() {
        return view('livewire.affiliate.dashboard')->layout('layouts.affiliate');
    }

}
