<?php

namespace App\Http\Livewire\Affiliate\Mailing;

use App\Models\Template;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Index extends Component {

    public $title   = 'Mailing';
    public $section = 'mailing';
    public $sort    = 'asc';
    public $templates;
    public $clients;
    public $table;
    public $template_id;
    public $template_name;
    public $template_description;
    public $template_content;
    public $content;
    public $confirm;
    public $mail_id;
    public $recipients;
    public $subject;
    public $batch = false;
    public $batch_size;
    public $batch_interval;

    protected $rules = [
        'template_name' => 'required',
        'template_description' => 'required',
        'template_content' => 'required'
    ];

    public function updated($propertyName){
        $this->validateOnly($propertyName);
    }

    public function mount() {
        $this->table     = 'templates';
        $this->templates = Template::mine();
        $this->clients   = User::my_clients();
    }

    public function render() {
        return view('livewire.affiliate.mailing.index')->layout('layouts.affiliate');
    }

    public function table($table) {
        $this->table = $table;
    }

    public function sort($table, $column) {
        $this->sort = ($this->sort == 'asc') ? 'desc' : 'asc';

        if($table == 'templates') {
            $this->templates = Template::mine($column, $this->sort);
        }

        if($table == 'clients') {
            $this->clients = User::my_clients($column, $this->sort);
        }
    }

    public function modalAddTemplate() {
        self::resetInputFields();
        $this->dispatchBrowserEvent('showAddTemplate');
    }

    public function modalEditTemplate($id) {
        $template = Template::get_my_template($id);

        if(!empty($template)) {
            $this->template_id          = $template->id;
            $this->template_name        = $template->name;
            $this->template_description = $template->description;
            $this->template_content     = $template->content;
            $this->content              = $template->content;

            $this->dispatchBrowserEvent('showEditTemplate', ['editor' => $this->content]);
        }
    }

    public function modalMailTemplate() {
        $this->dispatchBrowserEvent('showMailTemplate');
    }

    public function addTemplate() {
        $this->template_content = $this->content;

        $data = $this->validate([
            'template_name'        => 'required',
            'template_description' => 'required',
            'template_content'     => 'required'
        ]);

        Template::create([
            'user'        => Auth::user()->id,
            'name'        => $data['template_name'],
            'description' => $data['template_description'],
            'content'     => $data['template_content']
        ]);

        self::resetInputFields();

        $this->templates = Template::mine();

        session()->flash('successTemplate', trans('Template succesfully created'));
        $this->dispatchBrowserEvent('hideAddTemplate');
    }

    public function editTemplate() {
        $this->template_content = $this->content;

        $data = $this->validate([
            'template_id'          => 'required|numeric',
            'template_name'        => 'required',
            'template_description' => 'required',
            'template_content'     => 'required'
        ]);

        $template = Template::find($data['template_id']);

        if(!empty($template)) {
            $template->name        = $data['template_name'];
            $template->description = $data['template_description'];
            $template->content     = $data['template_content'];
            $template->save();
        }

        self::resetInputFields();

        $this->templates = Template::mine();

        session()->flash('successTemplate', trans('Template succesfully edited'));
        $this->dispatchBrowserEvent('hideEditTemplate');
    }

    public function confirm($id) {
        $this->confirm = $id;
        $this->dispatchBrowserEvent('confirmDelete');
    }

    public function delete() {
        Template::destroy($this->confirm);
        $this->templates = Template::mine();
        $this->confirm   = '';
    }

    public function mailTemplate() {
        \Log::debug('mailTemplate()');
        \Log::debug( 'recipients: '. $this->recipients );
        \Log::debug( 'subject: '. $this->subject );
        \Log::debug( 'batch: '. $this->batch );
        \Log::debug( 'batch_size: '. $this->batch_size );
        \Log::debug( 'batch_interval: '. $this->batch_interval );
    }

    private function resetInputFields() {
        $this->template_name        = '';
        $this->template_description = '';
        $this->template_content     = '';
        $this->content              = '';
    }


}
