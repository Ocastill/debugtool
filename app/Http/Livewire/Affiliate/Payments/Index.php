<?php

namespace App\Http\Livewire\Affiliate\Payments;

use Livewire\Component;

class Index extends Component {

    public $title   = 'Payments';
    public $section = 'payments';

    public function render() {
        return view('livewire.affiliate.payments.index')->layout('layouts.affiliate');
    }

}
