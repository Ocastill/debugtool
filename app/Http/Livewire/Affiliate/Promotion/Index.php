<?php

namespace App\Http\Livewire\Affiliate\Promotion;

use Livewire\Component;

class Index extends Component {

    public $title   = 'Promotion';
    public $section = 'promotion';

    public function render() {
        return view('livewire.affiliate.promotion.index')->layout('layouts.affiliate');
    }

}
