<?php

namespace App\Http\Livewire\Affiliate;

use Livewire\Component;

class Stats extends Component {

    public $title   = 'Hits';
    public $section = 'stats';

    public function render() {
        return view('livewire.affiliate.stats')->layout('layouts.affiliate');
    }

}
