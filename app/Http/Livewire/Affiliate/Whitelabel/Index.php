<?php

namespace App\Http\Livewire\Affiliate\Whitelabel;

use Livewire\Component;

class Index extends Component {

    public $title   = 'White label';
    public $section = 'whitelabel';

    public function render() {
        return view('livewire.affiliate.whitelabel.index')->layout('layouts.affiliate');
    }

}
