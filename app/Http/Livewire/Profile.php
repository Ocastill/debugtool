<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Profile extends Component {

    public $image;
    public $uploaded = false;
    public $tab;

    public function render() {
        $this->image = 'dec204377db71a2f84b2.jpg';

        return view('livewire.profile');
    }

}
