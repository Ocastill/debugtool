<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Topbar extends Component {

    public $profile_image;
    public $profile_name;
    public $email;

    public function render() {
        $this->profile_image = 'dec204377db71a2f84b2.jpg';
        $this->profile_name  = 'John Doe';
        $this->email         = 'email@example.com';

        return view('livewire.topbar');
    }

}
