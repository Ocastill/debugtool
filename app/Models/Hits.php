<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Hits extends Model {

    use HasFactory;

    protected $table = 'hits';

    public $timestamps = false;

    protected $fillable = [
        'affiliate',
        'client',
        'ip',
        'browser'
    ];

    public static function register($affiliate, $client) {
        return Hits::insertGetId(
            ['affiliate' => $affiliate,
             'client'    => $client,
             'ip'        => request()->ip(),
             'browser'   => request()->userAgent()
            ]
        );
    }

}
