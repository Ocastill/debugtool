<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Template extends Model {

    use HasFactory;

    protected $table = 'templates';

    public $timestamps = false;

    protected $fillable = [
        'user',
        'name',
        'description',
        'content'
    ];

    public static function mine($column = 'name', $sort = 'asc') {
        return Template::where('user', Auth::user()->id)->orderBy($column, $sort)->get();
    }

    public static function by_user($user) {
        return Template::where('user', $user)->orderBy('name', 'asc')->get();
    }

    public static function get_my_template($id) {
        return Template::where('user', Auth::user()->id)->where('id', $id)->first();
    }

}
