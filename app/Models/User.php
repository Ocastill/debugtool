<?php

namespace App\Models;

use App\Notifications\EmailVerificationNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
        'role',
        'affiliate_id',
        'ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function sendEmailVerificationNotification() {
        $this->notify(new EmailVerificationNotification());
    }

    public static function get_affiliates_id() {
        return User::where('role', 'affiliate')->whereNotNull('affiliate_id')->get()->pluck('affiliate_id');
    }

    public static function by_affiliate_id($affiliate) {
        return User::where('role', 'affiliate')->where('affiliate_id', $affiliate)->first();
    }

    public static function affiliate_not_exists($affiliate) {
        return User::where('role', 'affiliate')->where('affiliate_id', $affiliate)->doesntExist();
    }

    public static function client_not_exists($affiliate, $client) {
        return User::where('role', 'client')->where('parent_id', $affiliate)->where('id', $client)->doesntExist();
    }

    public static function my_clients($column = 'name', $sort = 'asc') {
        return User::where('role', 'client')->where('parent_id', Auth::user()->id)->orderBy($column, $sort)->get();
    }

}
