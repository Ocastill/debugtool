<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;

class EmailVerificationNotification extends VerifyEmail {

    public function toMail($notifiable) {
        $name = $notifiable->name;
        $link = $this->verificationUrl($notifiable);

        return (new MailMessage)
            ->view('mails.verification', ['name' => $name, 'link' => $link])
            ->subject(trans('view.Verify email address'));
    }

}
