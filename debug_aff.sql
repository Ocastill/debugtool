-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Gegenereerd op: 31 mei 2021 om 17:12
-- Serverversie: 5.7.28
-- PHP-versie: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `debug_aff`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `added_projects`
--

DROP TABLE IF EXISTS `added_projects`;
CREATE TABLE IF NOT EXISTS `added_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`,`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `added_projects`
--

INSERT INTO `added_projects` (`id`, `project_id`, `user_id`) VALUES
(1, 2, 8),
(2, 3, 8),
(12, 6, 8);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `bug`
--

DROP TABLE IF EXISTS `bug`;
CREATE TABLE IF NOT EXISTS `bug` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `by_user` int(11) DEFAULT '0',
  `project_id` int(11) DEFAULT NULL,
  `x_pos` varchar(255) DEFAULT NULL,
  `y_pos` varchar(255) DEFAULT NULL,
  `css3` varchar(255) NOT NULL,
  `assign_to` int(11) DEFAULT NULL,
  `status` tinyint(11) DEFAULT NULL,
  `prio` tinyint(4) DEFAULT NULL,
  `description` longtext,
  `browser` varchar(255) DEFAULT NULL,
  `time_spent` float DEFAULT NULL,
  `billable` tinyint(4) DEFAULT NULL,
  `window_size` varchar(85) DEFAULT NULL,
  `type_of_work` int(11) DEFAULT NULL,
  `operation_system` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `screenshot` varchar(255) DEFAULT NULL,
  `link_loomvideo` varchar(125) DEFAULT NULL,
  `order_prio` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `parent_id` (`attachment`),
  KEY `user` (`user_id`),
  KEY `assign` (`assign_to`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `bug`
--

INSERT INTO `bug` (`id`, `url`, `user_id`, `by_user`, `project_id`, `x_pos`, `y_pos`, `css3`, `assign_to`, `status`, `prio`, `description`, `browser`, `time_spent`, `billable`, `window_size`, `type_of_work`, `operation_system`, `parent_id`, `attachment`, `screenshot`, `link_loomvideo`, `order_prio`, `created_at`, `updated_at`) VALUES
(1, 'https://www.testdomain.com', 7, 0, 1, '122.312.32', '112.222.22', '.kop', NULL, 1, 2, 'dsdsdsddsds sdds sdsd dsds', 'internet', 12, NULL, '1233-2233', 2, '122.312.32', NULL, '1dd55536cbc045904589.png', NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-01 19:22:36', '2021-04-02 01:22:36'),
(2, 'https://www.testdomain.com', 7, 0, 1, '122.312.32', '112.222.22', '.kop', NULL, 1, 2, 'dsdsdsddsds sdds sdsd dsds', 'internet', 12, NULL, '1233-2233', 2, '122.312.32', 1, '1dd55536cbc045904589.png', NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-01 19:30:55', '2021-04-02 01:30:55'),
(3, 'https://www.testdomain.com', 7, 0, 1, '122.312.32', '112.222.22', '.kop', NULL, 1, 2, 'dsdsdsddsds sdds sdsd dsds', 'internet', 12, NULL, '1233-2233', 2, '122.312.32', 1, NULL, NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-01 19:31:17', '2021-04-02 01:31:17'),
(4, 'https://www.testdomain.com', 7, 0, 1, '122.312.32', '112.222.22', '.kop', NULL, 1, 2, 'dsdsdsddsds sdds sdsd dsds', 'internet', 12, NULL, '1233-2233', 2, '122.312.32', NULL, NULL, NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-01 19:31:47', '2021-04-02 01:31:47'),
(5, 'https://www.testdomain.com', 7, 0, 1, '122.312.32', '112.222.22', '.kop', NULL, 1, 2, 'dsdsdsddsds sdds sdsd dsds', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', NULL, NULL, NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-01 20:56:57', '2021-04-02 02:56:57'),
(6, 'https://www.testdomain.com', 7, 0, 1, '122.312.32', '112.222.22', '.kop', NULL, 1, 2, 'dsdsdsddsds sdds sdsd dsds', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', NULL, NULL, NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-01 20:57:06', '2021-04-02 02:57:06'),
(7, 'https://www.testdomain.com', 7, 21, 1, '122.312.32', '112.222.22', '.kop', NULL, 1, 2, 'dsdsdsddsds sdds sdsd dsds', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', NULL, NULL, NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-01 20:58:11', '2021-04-02 02:58:11'),
(8, 'https://www.testdomain.com', 7, 0, 1, '122.312.32', '112.222.22', '.kop', NULL, 1, 2, 'dsdsdsddsds sdds sdsd dsds', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', NULL, NULL, NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-01 21:00:11', '2021-04-02 03:00:11'),
(9, 'https://www.testdomain.com', 7, 21, 1, '122.312.32', '112.222.22', '.kop', NULL, 1, 2, 'dsdsdsddsds sdds sdsd dsds', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', NULL, NULL, NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-01 21:00:30', '2021-04-02 03:00:30'),
(10, 'https://www.testdomain.com', 7, 21, 1, '122.312.32', '112.222.22', '.kop', NULL, 1, 2, 'dsdsdsddsds sdds sdsd dsds', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', NULL, NULL, NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-01 21:01:08', '2021-04-02 03:01:08'),
(11, 'https://www.testdomain.com', 7, 21, 1, '122.312.32', '112.222.22', '.kop', NULL, 1, 2, 'dsdsdsddsds sdds sdsd dsds', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', NULL, NULL, NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-01 21:11:19', '2021-04-02 03:11:19'),
(12, 'https://www.testdomain.com', 7, 21, 1, '122.312.32', '112.222.22', '.kop', 0, 1, 2, 'Would you be so kind to check it again because when i hit this button nothing happens. Maybe something with javascript not sure!?', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', NULL, '1dd55536cbc045904589.png', NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-01 23:55:24', '2021-04-02 05:55:24'),
(13, 'https://www.testdomain.com', 7, 21, 1, '122.312.32', '112.222.22', '.kop', 0, 1, 2, 'Would you be so kind to check it again because when i hit this button nothing happens. Maybe something with javascript not sure!?', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', NULL, '11b5055aee8c2a5aa979.jpg', NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-02 00:00:17', '2021-04-02 06:00:17'),
(14, 'https://www.testdomain.com', 7, 21, 1, '122.312.32', '112.222.22', '.kop', 0, 1, 2, 'Would you be so kind to check it again because when i hit this button nothing happens. Maybe something with javascript not sure!?', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', NULL, 'b6530b7cae5854d465ac.jpg', NULL, 'https://www.test.nl/name_video.jpg', NULL, '2021-04-02 00:06:44', '2021-04-02 06:06:44'),
(15, 'https://www.lunch.nl/over-ons', 8, 21, 2, '122.312.32', '112.222.22', '.kop', 19, 1, 2, 'Wouldtt you be so kind to check it again because when i hit this button nothing happens. Maybe something with javascript not sure!?', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', NULL, 'b6530b7cae5854d465ac.jpg', NULL, 'https://www.test.nl/name_video.mp4', NULL, '2021-04-02 00:06:44', '2021-04-02 06:06:44'),
(16, 'https://www.lunch.nl/over-ons', 8, 21, 2, '122.312.32', '112.222.22', '.kop', 19, 1, 2, 'Would you be so kind to check it again because when i hit this button nothing happens. Maybe something with javascript not sure!?', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', 15, 'b6530b7cae5854d465ac.jpg', NULL, 'https://www.test.nl/name_video.mp4', NULL, '2021-04-02 00:06:44', '2021-04-02 06:06:44'),
(17, 'https://www.lunch.nl/over-ons', 8, 21, 2, '122.312.32', '112.222.22', '.kop', 19, 1, 2, 'Would you be so kind to check it again because when i hit this button nothing happens. Maybe something with javascript not sure!?', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', 15, 'b6530b7cae5854d465ac.jpg', NULL, 'https://www.test.nl/name_video.mp4', NULL, '2021-04-02 00:06:44', '2021-04-02 06:06:44'),
(18, 'https://www.lunch.nl/over-ons', 8, 21, 2, '122.312.32', '112.222.22', '.kop', 19, 1, 2, 'Wouldt you be so kind to check it again because when i hit this button nothing happens. Maybe something with javascript not sure!?', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', 15, 'b6530b7cae5854d465ac.jpg', NULL, 'https://www.test.nl/name_video.mp4', NULL, '2021-04-02 00:06:44', '2021-04-02 06:06:44'),
(19, 'https://www.lunch.nl/over-ons', 8, 21, 2, '122.312.32', '112.222.22', '.row', 19, 1, 2, 'Would x you be so kind to check it again because when i hit this button nothing happens. Maybe something with javascript not sure!?', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', 15, 'b6530b7cae5854d465ac.jpg', NULL, 'https://www.test.nl/name_video.mp4', NULL, '2021-04-02 00:06:44', '2021-04-02 06:06:44'),
(20, 'https://www.lunch.nl/how-it-works', 8, 21, 2, '122.312.32', '112.222.22', '.row', 19, 1, 2, 'how it works, i dont know but can you replace the images?', 'internet explorer', 12, NULL, '1233-2233', 2, '122.312.32', NULL, 'b6530b7cae5854d465ac.jpg', NULL, 'https://www.test.nl/name_video.mp4', NULL, '2021-04-02 00:06:44', '2021-04-02 06:06:44');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=240 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `hits`
--

DROP TABLE IF EXISTS `hits`;
CREATE TABLE IF NOT EXISTS `hits` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `affiliate` bigint(20) UNSIGNED NOT NULL,
  `client` bigint(20) UNSIGNED DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `browser` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index_affiliate` (`affiliate`),
  KEY `index_client` (`client`),
  KEY `index_ip` (`ip`),
  KEY `index_browser` (`browser`),
  KEY `index_created_at` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `hits`
--

INSERT INTO `hits` (`id`, `affiliate`, `client`, `ip`, `browser`, `created_at`) VALUES
(1, 39, 40, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', '2021-04-20 18:06:34'),
(2, 39, 40, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', '2021-04-20 18:08:26');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ltm_translations`
--

DROP TABLE IF EXISTS `ltm_translations`;
CREATE TABLE IF NOT EXISTS `ltm_translations` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `group` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `key` text COLLATE utf8mb4_bin NOT NULL,
  `value` text COLLATE utf8mb4_bin,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Gegevens worden geëxporteerd voor tabel `ltm_translations`
--

INSERT INTO `ltm_translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 0, 'en', 'view', 'Your account has been blocked. Please contact the webmaster if you think this might be incorrect.', 'Your account has been blocked. Please contact the webmaster if you think this is incorrect.', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(2, 0, 'en', 'view', 'Your account has been deleted. Please contact the webmaster if you think this might be incorrect.', 'Your account has been deleted. Please contact the webmaster if you think this is incorrect.', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(3, 0, 'en', 'view', 'Your account is unactive. You can activate your account by clicking this:', 'Your account is unactive. You can activate your account by clicking this', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(4, 0, 'en', 'view', 'link', 'link', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(5, 0, 'en', 'controller', 'Registration successfull. An e-mail has been sent to activate your account. Did not receive anything? Please also check the promotion or the spam tab.', 'Registration successfull. An e-mail has been sent to activate your account. Did not receive anything? Please also check the promotion or the spam tab.', '2021-03-30 04:42:45', '2021-03-30 04:58:42'),
(6, 0, 'en', 'view', 'register_content', '<p>Thank you for registering on our website, you can confirm your account at  the following url:</p>', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(7, 0, 'en', 'view', 'Confirm email', 'Confirm email', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(8, 0, 'en', 'view', 'Your registration', 'Your registration', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(9, 0, 'en', 'view', 'Security alert', 'Security alert', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(10, 0, 'en', 'view', 'Someone has automatically logged in with your account. If it wasn\\\'t you, review the activity and protect your account.', 'Someone has automatically logged in with your account. If it wasn\\\'t you, review the activity and protect your account.', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(11, 0, 'en', 'view', 'Go to my account', 'Go to my account', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(12, 0, 'en', 'controller', 'Your account has been succesfully activated. You can now login! ', 'Your account has been succesfully activated. You can now login!', '2021-03-30 04:42:45', '2021-03-30 04:58:42'),
(13, 0, 'en', 'controller', 'Your email has been succesfully confirmed. You can now login! ', 'Your email has been succesfully confirmed. You can now login!', '2021-03-30 04:42:45', '2021-03-30 04:58:42'),
(14, 0, 'en', 'controller', 'Your account has not been activated, the activation url is not correct. Please check the url and try again.', 'Your account has not been activated, the activation url is not correct. Please check the url and try again.', '2021-03-30 04:42:45', '2021-03-30 04:58:42'),
(15, 0, 'en', 'view', '<p>You requested a new password for Debugtool, you can now login with the following details:<br/><br/><b>Email:</b> :email <br/><b>Password: </b> :password <br/><br/></p><p>With kind regards,<br/>Team Debugtool. </p>', '<p>You requested a new password for Debugtool, you can login with the following details:<br/><br/><b>Email:</b> :email <br/><b>Password: </b> :password <br/><br/></p><p>With kind regards,<br/>Team Debugtool. </p>', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(16, 0, 'en', 'view', 'Your inlog details on Debugtool', 'Your login details on Debugtool', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(17, 0, 'en', 'view', 'Success', 'Success', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(18, 0, 'en', 'view', 'A new password has been sent.', 'A new password has been sent.', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(19, 0, 'en', 'view', 'Failed', 'Failed', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(20, 0, 'en', 'view', 'E-mail not found in database.', 'E-mail not found in database.', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(21, 0, 'en', 'view', 'You can verify your email address by clicking the button below', 'You can verify your email address by clicking the button below', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(22, 0, 'en', 'view', 'Activation link for Debugtool', 'Activation link for Debugtool', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(23, 0, 'en', 'view', 'Email has been sent again, you couldnot find it? Please also check the promotion or spam folder.', 'Email has been sent. Please check also the promotion or spam folder.', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(24, 0, 'en', 'view', 'Your account is already activated.', 'Your account is already activated.', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(25, 0, 'en', 'view', 'Your account has been blocked.', 'Your account has been blocked.', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(26, 0, 'en', 'view', 'The :attribute field is required', 'The :attribute field is required', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(27, 0, 'en', 'view', 'The :attribute field must be a number', 'The :attribute field must be a number', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(28, 0, 'en', 'view', 'Please fill in a valid email address', 'Please fill in a valid email address', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(29, 0, 'en', 'view', 'This email already exists in our datebase.', 'This email already exists in our datebase.', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(30, 0, 'en', 'view', 'Username already exists, please choose an other one.', 'Username already exists, please choose an other one.', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(31, 0, 'en', 'view', 'name', 'name', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(32, 0, 'en', 'view', 'country', 'country', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(33, 0, 'en', 'view', 'province', 'province', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(34, 0, 'en', 'view', 'city', 'city', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(35, 0, 'en', 'view', 'address', 'address', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(36, 0, 'en', 'view', 'postalcode', 'postalcode', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(37, 0, 'en', 'view', 'email', 'email', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(38, 0, 'en', 'view', 'company', 'company', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(39, 0, 'en', 'view', 'password', 'password', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(40, 0, 'en', 'view', 'password-confirm', 'password-confirm', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(41, 0, 'en', 'view', 'Email', 'Email', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(42, 0, 'en', 'view', 'Password', 'Password', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(43, 0, 'en', 'view', 'Login', 'Login', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(44, 0, 'en', 'view', 'Keep me signed in', 'Keep me signed in', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(45, 0, 'en', 'view', 'Forgot Password', 'Forgot Password', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(46, 0, 'en', 'view', 'Not a member ?', 'Not a member ?', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(47, 0, 'en', 'view', 'Create new account', 'Create new account', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(48, 0, 'en', 'view', 'Conditions', 'Conditions', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(49, 0, 'en', 'view', 'Help', 'Help', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(50, 0, 'en', 'view', 'Terms', 'Terms', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(51, 0, 'en', 'view', 'All rights reserved.', 'All rights reserved.', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(52, 0, 'en', 'view', 'Optimize your workflow with Debugtool.Save money and time!', 'Optimize your workflow with Debugtool.Save money and time!', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(53, 0, 'en', 'view', 'Uw inschrijving', 'Your insciption', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(54, 0, 'en', 'view', 'Hallo', 'Hi', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(55, 0, 'en', 'view', 'Uitschrijven', 'No more newsletters ?', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(56, 0, 'en', 'view', 'Request a new password', 'Request a new password', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(57, 0, 'en', 'view', 'Send', 'Send', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(58, 0, 'en', 'view', 'Register', 'Register', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(59, 0, 'en', 'view', 'Username', 'Username', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(60, 0, 'en', 'view', 'Confirm Password', 'Confirm Password', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(61, 0, 'en', 'view', 'I agree to the terms', 'I agree to the terms', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(62, 0, 'en', 'view', 'Already have and account ?', 'Already have and account ?', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(63, 0, 'en', 'view', 'Login ?', 'Login', '2021-03-30 04:42:45', '2021-03-30 05:52:28'),
(64, 0, 'en', '_json', 'view.Your account has been blocked. Please contact the webmaster if you think this might be incorrect.', 'Your account has been blocked. Please contact the webmaster if you think this might be incorrect.', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(65, 0, 'en', '_json', 'view.Your account has been deleted. Please contact the webmaster if you think this might be incorrect.', 'Your account has been deleted. Please contact the webmaster if you think this might be incorrect.', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(66, 0, 'en', '_json', 'view.Your account is unactive. You can activate your account by clicking this:', 'Your account is unactive. You can activate your account by clicking this:', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(67, 0, 'en', '_json', 'controller.Registration successfull. An e-mail has been sent to activate your account. Did not receive anything? Please also check the promotion or the spam tab.', 'Registration successfull. An e-mail has been sent to activate your account. Did not receive anything? Please also check the promotion or the spam tab.', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(68, 0, 'en', '_json', 'view.Confirm email', 'Confirm email', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(69, 0, 'en', '_json', 'view.Your registration', 'Your registration on Debugtool', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(70, 0, 'en', '_json', 'view.Security alert', 'Security alert', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(71, 0, 'en', '_json', 'view.Go to my account', 'Go to my account', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(72, 0, 'en', '_json', 'controller.Your account has been succesfully activated. You can now login! ', 'Your account has been succesfully activated. You can now login!', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(73, 0, 'en', '_json', 'controller.Your email has been succesfully confirmed. You can now login! ', 'Your email has been succesfully confirmed. You can now login!', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(74, 0, 'en', '_json', 'controller.Your account has not been activated, the activation url is not correct. Please check the url and try again.', 'Your account has not been activated, the activation url is not correct. Please check the url and try again.', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(75, 0, 'en', '_json', 'view.<p>You requested a new password for Debugtool, you can now login with the following details:<br/><br/><b>Email:</b> :email <br/><b>Password: </b> :password <br/><br/></p><p>With kind regards,<br/>Team Debugtool. </p>', '<p>You requested a new password for Debugtool, you can login with the following details:<br/><br/><b>Email:</b> :email <br/><b>Password: </b> :password <br/><br/></p><p>With kind regards,<br/>Team Debugtool. </p>', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(76, 0, 'en', '_json', 'view.Your inlog details on Debugtool', 'Your inlog details on Debugtool', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(77, 0, 'en', '_json', 'view.A new password has been sent.', 'A new password has been sent.', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(78, 0, 'en', '_json', 'view.E-mail not found in database.', 'E-mail not found in database.', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(79, 0, 'en', '_json', 'view.You can verify your email address by clicking the button below', 'You can verify your email address by clicking the button below', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(80, 0, 'en', '_json', 'view.Activation link for Debugtool', 'Activation link for Debugtool', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(81, 0, 'en', '_json', 'view.Email has been sent again, you couldnot find it? Please also check the promotion or spam folder.', 'Email has been sent again.Please also check the promotion or spam folder.', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(82, 0, 'en', '_json', 'view.Your account is already activated.', 'Your account is already activated.', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(83, 0, 'en', '_json', 'view.Your account has been blocked.', 'Your account has been blocked.', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(84, 0, 'en', '_json', 'view.The :attribute field is required', 'The :attribute field is required', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(85, 0, 'en', '_json', 'view.The :attribute field must be a number', 'The :attribute field must be a number', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(86, 0, 'en', '_json', 'view.Please fill in a valid email address', 'Please fill in a valid email address', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(87, 0, 'en', '_json', 'view.This email already exists in our datebase.', 'This email already exists in our datebase.', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(88, 0, 'en', '_json', 'view.Username already exists, please choose an other one.', 'Username already exists, please choose an other one.', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(89, 0, 'en', '_json', 'Login', 'Login', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(90, 0, 'en', '_json', 'E-Mail Address', 'E-Mail Address', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(91, 0, 'en', '_json', 'Password', 'Password', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(92, 0, 'en', '_json', 'Remember Me', 'Remember Me', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(93, 0, 'en', '_json', 'Forgot Your Password?', 'Forgot Your Password?', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(94, 0, 'en', '_json', 'Confirm Password', 'Confirm Password', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(95, 0, 'en', '_json', 'Please confirm your password before continuing.', 'Please confirm your password before continuing.', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(96, 0, 'en', '_json', 'Reset Password', 'Reset Password', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(97, 0, 'en', '_json', 'Send Password Reset Link', 'Send Password Reset Link', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(98, 0, 'en', '_json', 'Register', 'Register', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(99, 0, 'en', '_json', 'Name', 'Name', '2021-03-30 04:42:45', '2021-03-30 05:50:44'),
(100, 0, 'en', '_json', 'Verify Your Email Address', 'Verify Your Email Address', '2021-03-30 04:42:45', '2021-03-30 05:50:44');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `mail_notification`
--

DROP TABLE IF EXISTS `mail_notification`;
CREATE TABLE IF NOT EXISTS `mail_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `once_a_day` int(11) DEFAULT NULL,
  `once_a_week` int(11) DEFAULT NULL,
  `real_time` int(11) DEFAULT NULL,
  `newsletter` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `mail_notification`
--

INSERT INTO `mail_notification` (`id`, `user_id`, `once_a_day`, `once_a_week`, `real_time`, `newsletter`) VALUES
(1, 19, 0, 1, 0, 1),
(2, 8, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2021_04_19_161828_create_sessions_table', 1),
(7, '2014_04_02_193005_create_translations_table', 2),
(8, '2019_05_03_000001_create_customer_columns', 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `prio`
--

DROP TABLE IF EXISTS `prio`;
CREATE TABLE IF NOT EXISTS `prio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `prio`
--

INSERT INTO `prio` (`id`, `name`) VALUES
(1, 'Low'),
(2, 'Medium'),
(3, 'High');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `project`
--

DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(85) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `screenshot` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `project`
--

INSERT INTO `project` (`id`, `name`, `url`, `user_id`, `client_id`, `screenshot`, `status`, `active`, `created_at`, `updated_at`) VALUES
(1, 'tesdomain', 'www.testdomain.com', 7, 7, NULL, 1, 1, '2020-09-03 16:03:52', '2020-09-03 16:03:52'),
(2, 'Lunch.nl', 'www.lunch.nl', 8, 21, 'screenshot-bfd9a34028371c676608.jpg', 1, 1, '2021-04-07 19:11:28', '2021-04-07 19:11:28'),
(3, 'Fishguppy', 'www.fishguppy.nl', 8, 79, 'screenshot-4d2169ff92c4ebb30787.jpg', 1, 1, '2021-04-08 15:14:25', '2021-04-08 15:14:25'),
(5, 'Tippelstraat', 'www.tippelstraat.nl', 8, 16, '', 1, 0, '2021-04-08 15:32:20', '2021-04-09 13:38:35'),
(7, 'Ruilplek', 'www.ruilplek.nl', 8, 79, 'screenshot-808264303f55cfa09317.jpg', 1, 1, '2021-04-09 11:21:14', '2021-04-09 11:21:14'),
(6, 'Flirtonline', 'www.flirtonline.nl', 8, 79, '', 1, 1, '2021-04-08 20:25:07', '2021-04-09 13:38:47');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `project_notification`
--

DROP TABLE IF EXISTS `project_notification`;
CREATE TABLE IF NOT EXISTS `project_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `project_user`
--

DROP TABLE IF EXISTS `project_user`;
CREATE TABLE IF NOT EXISTS `project_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `developer_id` (`user_id`),
  KEY `project_id` (`project_id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `project_user`
--

INSERT INTO `project_user` (`id`, `user_id`, `project_id`) VALUES
(1, 7, 1),
(2, 19, 2),
(3, 20, 2),
(4, 21, 2),
(24, 25, 3),
(23, 19, 3),
(16, 22, 5),
(15, 20, 5),
(14, 16, 5),
(13, 19, 5),
(12, 8, 5),
(22, 8, 3),
(25, 8, 6),
(26, 19, 6),
(30, 20, 7),
(29, 8, 7),
(31, 22, 7);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('DItyZOB4XHEuU9sUqnVdYOkkUMFPSbeahOMU8gbS', 7, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoicDZJaE05RVRwc3VleDE4WHpVZVh1S3IxV3Y1ZDhyTUt3YjZEczZxWSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHBzOi8vZGVidWd0b29sLmxvY2FsL2xvZ2luIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6Nzt9', 1622476531),
('OqbMn7Kwqd6DATrZgxP0ZndGElOkBA5yq9Sb3RPi', 7, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiOENHN2pzSk1LQ0JrSnl0Q2hkcWxDNDJGUUJWSXN6c2RoR2hxNno3aiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDM6Imh0dHBzOi8vZGVidWd0b29sLmxvY2FsL2FmZmlsaWF0ZS9kYXNoYm9hcmQiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aTo3O3M6MTc6InBhc3N3b3JkX2hhc2hfd2ViIjtzOjYwOiIkMnkkMTAkSnVhSW5UblNtYW5yRG5FWWJIb0NldUQwUTN0WndwVnJBdHNNMzBYYkp5WUk5Z2JrYkdvRUMiO3M6MjE6InBhc3N3b3JkX2hhc2hfc2FuY3R1bSI7czo2MDoiJDJ5JDEwJEp1YUluVG5TbWFuckRuRVliSG9DZXVEMFEzdFp3cFZyQXRzTTMwWGJKeVlJOWdia2JHb0VDIjt9', 1622476544),
('xpKfq8YAa6leuylCkYEPzFJZbfI5QtNB9yKmn9gt', 7, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiejk0V3MwUDZ0RzJOejJ6MmNDckVxRndMTHkxdFVpRFdzTGlPdGI2UiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mzc6Imh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9hZmZpbGlhdGUvc3RhdHMiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aTo3O3M6MTc6InBhc3N3b3JkX2hhc2hfd2ViIjtzOjYwOiIkMnkkMTAkSnVhSW5UblNtYW5yRG5FWWJIb0NldUQwUTN0WndwVnJBdHNNMzBYYkp5WUk5Z2JrYkdvRUMiO3M6MjE6InBhc3N3b3JkX2hhc2hfc2FuY3R1bSI7czo2MDoiJDJ5JDEwJEp1YUluVG5TbWFuckRuRVliSG9DZXVEMFEzdFp3cFZyQXRzTTMwWGJKeVlJOWdia2JHb0VDIjt9', 1622479755);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `status`
--

INSERT INTO `status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Pending'),
(3, 'Reopened'),
(4, 'Closed');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `subscription`
--

DROP TABLE IF EXISTS `subscription`;
CREATE TABLE IF NOT EXISTS `subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` double DEFAULT NULL,
  `description` mediumtext,
  `number_of_urls` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `subscription`
--

INSERT INTO `subscription` (`id`, `amount`, `description`, `number_of_urls`, `status`, `created_at`, `deleted_at`) VALUES
(1, 30, 'basic	', 50, 1, '2020-09-03 16:03:52', '2021-04-22 00:00:00');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subscriptions_user_id_stripe_status_index` (`user_id`,`stripe_status`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `user_id`, `name`, `stripe_id`, `stripe_status`, `stripe_plan`, `quantity`, `trial_ends_at`, `ends_at`, `created_at`, `updated_at`) VALUES
(1, 8, 'default', 'sub_JFLFGLwexBNQqO', 'active', 'price_1IcDwGGE1tm3GytgwxBJtxxS', 1, NULL, NULL, '2021-04-05 16:26:12', '2021-04-05 16:26:12');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `subscription_api`
--

DROP TABLE IF EXISTS `subscription_api`;
CREATE TABLE IF NOT EXISTS `subscription_api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `subscription_id` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `apikey` varchar(85) DEFAULT NULL,
  `status` smallint(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subscription_id` (`subscription_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `subscription_api`
--

INSERT INTO `subscription_api` (`id`, `user_id`, `subscription_id`, `start_date`, `end_date`, `apikey`, `status`, `created_at`, `updated_at`) VALUES
(1, 7, 1, '2021-06-23 00:00:00', '2021-04-29 00:00:00', 'Afg-786-@5654', 1, '2021-04-01 00:00:00', '2021-04-15 00:00:00');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `subscription_items`
--

DROP TABLE IF EXISTS `subscription_items`;
CREATE TABLE IF NOT EXISTS `subscription_items` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subscription_id` bigint(20) UNSIGNED NOT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subscription_items_subscription_id_stripe_plan_unique` (`subscription_id`,`stripe_plan`),
  KEY `subscription_items_stripe_id_index` (`stripe_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `subscription_items`
--

INSERT INTO `subscription_items` (`id`, `subscription_id`, `stripe_id`, `stripe_plan`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 1, 'si_JFLFGZfP0lHWcb', 'price_1IcDwGGE1tm3GytgwxBJtxxS', 1, '2021-04-05 16:26:12', '2021-04-05 16:26:12');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `templates`
--

DROP TABLE IF EXISTS `templates`;
CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` tinytext,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `templates`
--

INSERT INTO `templates` (`id`, `user`, `name`, `description`, `content`) VALUES
(1, 39, 'Example', 'This is the first template for example', 'asdasdasd'),
(4, 39, 'Newsletter', 'Template for newsletter email', '<p>asdasdasdasdasdasd 2</p>');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `type_of_work`
--

DROP TABLE IF EXISTS `type_of_work`;
CREATE TABLE IF NOT EXISTS `type_of_work` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `type_of_work`
--

INSERT INTO `type_of_work` (`id`, `name`) VALUES
(1, 'design'),
(2, 'frontend'),
(3, 'code'),
(4, 'question');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(85) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(85) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(65) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(85) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postalcode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(65) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `affiliate_id` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(65) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `company` varchar(85) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vatno` varchar(85) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `blocked` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_stripe_id_index` (`stripe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `name`, `first_name`, `last_name`, `role`, `address`, `postalcode`, `city`, `country`, `parent_id`, `affiliate_id`, `ip`, `last_login`, `company`, `vatno`, `deleted`, `blocked`, `active`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `profile_image`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`, `created_at`, `updated_at`) VALUES
(7, 'Pablo de Zwaan', NULL, NULL, 'member', NULL, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '2021-04-14 11:13:26', NULL, NULL, NULL, NULL, NULL, 'pfzwaan@gmail.com', '2021-05-24 17:36:36', '$2y$10$JuaInTnSmanrDnEYbHoCeuD0Q3tZwpVrAtsM30XbJyYI9gbkbGoEC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-31 04:23:42', '2021-05-24 17:36:36'),
(8, 'Zwano', NULL, NULL, 'member', NULL, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '2021-04-14 11:13:35', 'SRIservices Mexico', NULL, NULL, NULL, NULL, 'pfzwaan+2@gmail.com', NULL, '$2y$10$JuaInTnSmanrDnEYbHoCeuD0Q3tZwpVrAtsM30XbJyYI9gbkbGoEC', NULL, NULL, NULL, NULL, NULL, 'dec204377db71a2f84b2.jpg', 'cus_JFLF14NQcSMXPU', 'visa', '4242', '2021-04-12 16:26:08', '2021-04-03 05:23:55', '2021-04-14 16:13:35'),
(15, '', 'Johan', 'Willemsen', 'client', NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pfzwaan+999@gmail.com', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-07 20:46:37', '2021-04-07 20:46:37'),
(16, '', 'Ronnie', 'Kivits', 'client', NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'info@xplow.nl', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-07 20:55:15', '2021-04-07 20:55:15'),
(17, '', 'Reyer', 'ten Napel', 'client', NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'info@pixxoo.nl', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-07 20:56:30', '2021-04-07 20:56:30'),
(18, '', 'Danny', 'Bijl', 'client', NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pfzwaan+10000@gmail.com', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-07 20:58:21', '2021-04-07 20:58:21'),
(19, '', 'Eduardo ', 'Rangel', 'programmer', NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rangelbravoeduardo@gmail.com', NULL, '$2y$10$JuaInTnSmanrDnEYbHoCeuD0Q3tZwpVrAtsM30XbJyYI9gbkbGoEC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-07 22:17:51', '2021-04-11 21:18:44'),
(20, '', 'Andrea', 'Castillo', 'designer', NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'andreacastillo783@gmail.com', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-07 22:22:38', '2021-04-07 22:22:38'),
(21, '', 'Robin', 'Lunch', 'client', NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pfzwaan+777@gmail.com', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-08 00:06:45', '2021-04-08 00:06:45'),
(22, '', 'Cristobal', 'Castillo', 'programmer', NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yokhristoff@gmail.com', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-08 16:18:57', '2021-04-08 16:18:57'),
(23, '', 'Paul', 'de Zwaan', 'project manager', NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pfzwaan+90@gmail.com', NULL, '$2y$10$JuaInTnSmanrDnEYbHoCeuD0Q3tZwpVrAtsM30XbJyYI9gbkbGoEC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-08 16:19:28', '2021-04-11 04:29:23'),
(24, '', 'Daniel', 'Terres', 'designer', NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pfzwaan+33@gmail.com', NULL, '$2y$10$JuaInTnSmanrDnEYbHoCeuD0Q3tZwpVrAtsM30XbJyYI9gbkbGoEC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-08 17:02:28', '2021-04-11 03:22:52'),
(25, '', 'Hublester', 'Ludoza', 'designer', NULL, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pfzwaan+88@gmail.com', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-08 17:07:32', '2021-04-08 17:07:32'),
(39, 'John', NULL, 'Doe', 'affiliate', NULL, NULL, NULL, NULL, NULL, '9616201756', '::1', NULL, NULL, NULL, NULL, NULL, NULL, 'john.doe@email.com', '2021-04-20 21:29:09', '$2y$10$jl6HBQZJLt4xjYLyWvjwMeRlIwmA30l87WWpZSF8av6pUt2zaUq3O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-20 21:26:28', '2021-04-20 21:29:09'),
(40, 'Jane', NULL, 'Doe', 'client', NULL, NULL, NULL, NULL, 39, NULL, '::1', NULL, NULL, NULL, NULL, NULL, NULL, 'jane.doe@email.com', '2021-04-20 21:32:00', '$2y$10$jl6HBQZJLt4xjYLyWvjwMeRlIwmA30l87WWpZSF8av6pUt2zaUq3O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-20 21:26:28', '2021-04-20 21:29:09');

--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `hits`
--
ALTER TABLE `hits`
  ADD CONSTRAINT `stats_affiliate_foreign` FOREIGN KEY (`affiliate`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `stats_client_foreign` FOREIGN KEY (`client`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Beperkingen voor tabel `templates`
--
ALTER TABLE `templates`
  ADD CONSTRAINT `templates_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
