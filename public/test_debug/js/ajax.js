$( document ).ready(function() {

      $("form.debugtoolform").submit(function(){
          event.preventDefault();
          $('.submit').hide();
          var formData = new FormData(this);
          $.ajax({
              url: "http://debugapp.soonlive.nl/addbug",
              type:"post",
              dataType: "json",
              crossDomain : true,
              data: formData,
              async: false,
              beforeSend: function(){
                  $('#preloader').show();
              },
              complete: function(){   
                  $('#preloader').hide();
              },
              success: function( data ) {
                  if(data.error){
                      console.log(data.error);
                      $('.submit').show();
                  }else{
                      //submit
                      console.log(data.success);
                      $('.submit').show();
                  }
              },
              cache: false,
              contentType: false,
              processData: false
          });
      });

  $(document).on('click','.get_bug' ,function(event){
      event.preventDefault();
      $.ajax({
          url: "http://debugapp.soonlive.nl/getBugById",
          type:"post",
          dataType: "json",
          data: {
              'bug_id': 3,
              'domain': 'www.testdomain.com'
          },
          beforeSend: function(){
              $('#preloader').show();
          },
          complete: function(){
              $('#preloader').hide();
          },
          success: function( data ) {
              if(data.error){
                  console.log(data.error);
              }else{
                  console.log(data.success);
              }
          }
      });
  });

  $(document).on('click','.get_bug_url' ,function(event){
      event.preventDefault();
      $.ajax({
          url: "http://debugapp.soonlive.nl/getBugsByUrl",
          type:"post",
          dataType: "json",
          data: {
              'domain': 'www.testdomain.com',
              'url': 'https://www.testdomain.com'
          },
          beforeSend: function(){
              $('#preloader').show();
          },
          complete: function(){
              $('#preloader').hide();
          },
          success: function( data ) {
              if(data.error){
                  console.log(data.error);
              }else{
                  console.log(data.success);
              }
          }
      });
  });

  $(document).on('click','.get_all_bugs_project' ,function(event){
      event.preventDefault();
      $.ajax({
          url: "http://debugapp.soonlive.nl/getAllBugsByDomain",
          type:"post",
          dataType: "json",
          data: {
              'domain': 'www.testdomain.com'
          },
          beforeSend: function(){
              $('#preloader').show();
          },
          complete: function(){
              $('#preloader').hide();
          },
          success: function( data ) {
              if(data.error){
                  console.log(data.error);
              }else{
                  console.log(data.success);
              }
          }
      });
  });

  $(document).on('click','.get_all_members_of_project' ,function(event){
      event.preventDefault();
      $.ajax({
          url: "http://debugapp.soonlive.nl/getAllMembersOfProject",
          type:"post",
          dataType: "json",
          data: {
              'domain': 'www.testdomain.com'
          },
          beforeSend: function(){
              $('#preloader').show();
          },
          complete: function(){
              $('#preloader').hide();
          },
          success: function( data ) {
              if(data.error){
                  console.log(data.error);
              }else{
                  console.log(data.success);
              }
          }
      });
  });

  $(document).on('click','.get_stats_of_project' ,function(event){
      event.preventDefault();
      $.ajax({
          url: "http://debugapp.soonlive.nl/getStatisticsOfProject",
          type:"post",
          dataType: "json",
          data: {
              'domain': 'www.testdomain.com'
          },
          beforeSend: function(){
              $('#preloader').show();
          },
          complete: function(){
              $('#preloader').hide();
          },
          success: function( data ) {
              if(data.error){
                  console.log(data.error);
              }else{
                  console.log(data.success);
              }
          }
      });
  });

  $(document).on('click','.get_type_of_work' ,function(event){
      event.preventDefault();
      $.ajax({
          url: "http://debugapp.soonlive.nl/getTypeOfWork",
          type:"post",
          dataType: "json",
          data: {
              'domain': 'www.testdomain.com'
          },
          beforeSend: function(){
              $('#preloader').show();
          },
          complete: function(){
              $('#preloader').hide();
          },
          success: function( data ) {
              if(data.error){
                  console.log(data.error);
              }else{
                  console.log(data.success);
              }
          }
      });
  });

  $(document).on('click','.get_statussen' ,function(event){
      event.preventDefault();
      $.ajax({
          url: "http://debugapp.soonlive.nl/getStatussen",
          type:"post",
          dataType: "json",
          data: {
              'domain': 'www.testdomain.com'
          },
          beforeSend: function(){
              $('#preloader').show();
          },
          complete: function(){
              $('#preloader').hide();
          },
          success: function( data ) {
              if(data.error){
                  console.log(data.error);
              }else{
                  console.log(data.success);
              }
          }
      });
  });

  $(document).on('click','.get_prio' ,function(event){
      event.preventDefault();
      $.ajax({
          url: "http://debugapp.soonlive.nl/getPrio",
          type:"post",
          dataType: "json",
          data: {
              'domain': 'www.testdomain.com'
          },
          beforeSend: function(){
              $('#preloader').show();
          },
          complete: function(){
              $('#preloader').hide();
          },
          success: function( data ) {
              if(data.error){
                  console.log(data.error);
              }else{
                  console.log(data.success);
              }
          }
      });
  });

  $(document).on('click','.notify' ,function(event){
      event.preventDefault();
      let teammembers = [7];
      $.ajax({
          url: "http://debugapp.soonlive.nl/notifyTeamMembers",
          type:"post",
          dataType: "json",
          data: {
              'domain': 'www.testdomain.com',
              'url': 'http://www.testdomain.com',
              'teammembers': teammembers,
              'comment': 'Do you think you will be able to finish this today?'
          },
          beforeSend: function(){
              $('#preloader').show();
          },
          complete: function(){
              $('#preloader').hide();
          },
          success: function( data ) {
              if(data.error){
                  console.log(data.error);
              }else{
                  console.log(data.success);
              }
          }
      });
  });

});
