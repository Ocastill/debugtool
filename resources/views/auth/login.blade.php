@extends('layouts.auth')

@section('content')
    <div class="container-scroller login">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper mt-0 d-flex align-items-center auth auth-bg-1 theme-one">
                <div class="row w-100">
                    <div class="col-md-6 col-lg-4 mx-auto">
                        <div class="auto-form-wrapper">
                            <img src="{{ asset('debugadmin/assets/images/menonlogin.png') }}" class="setmegood" />
                            <a class="navbar-brand" href="#"><img src="{{ asset('website/build/public/img/logo-dark.png') }}" alt="Debugtool - The Only Agency Management Platform Dedicated To Website Design & Development"></a>

                            @if(!empty($success))
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!!$success!!}
                                </div>
                            @endif

                            @if(!empty($error))
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!!$error!!}
                                </div>
                            @endif

                            @if(count($errors))
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <ul class="list-unstyled mb-0" >
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form method="POST" action="{{ route('login') }}" autocomplete="off">
                                @csrf
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="email" onfocus="this.removeAttribute('readonly');" readonly  value="{{old('email')}}" class="form-control" placeholder="{{trans('view.Email')}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="password"  autocomplete="new-password" onfocus="this.removeAttribute('readonly');" readonly name="password" class="form-control" placeholder="{{trans('view.Password')}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <a href="#" class="text-small forgot-password text-black">{{trans('view.Forgot Password')}}</a>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary submit-btn btn-block">{{trans('view.Login')}}</button>
                                </div>
                                <div class="form-group d-flex justify-content-between">
                                    <div class="form-check form-check-flat mt-0">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" checked> {{trans('view.Keep me signed in')}} </label>
                                    </div>
                                </div>

                                <div class="text-block text-center my-3">
                                    <span class="text-small font-weight-semibold">{{trans('view.Not a member ?')}}</span>
                                    <a href="{{ route('register') }}" class="text-black text-small">{{trans('view.Create new account')}}</a>
                                </div>
                            </form>
                        </div>
                        <ul class="auth-footer">
                            <li>
                                <a href="#">{{__('Terms of use')}}</a>
                            </li>
                            <li>
                                <a href="#">{{__('Privacy Policy')}}</a>
                            </li>
                            <li>
                                <a href="#">{{__('Cookies Policy')}}</a>
                            </li>
                        </ul>
                        <p class="footer-text text-center">{{trans('view.All rights reserved.')}} Copyright ©{{ date('Y') }}</p>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
@endsection
