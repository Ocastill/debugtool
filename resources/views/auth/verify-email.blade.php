@extends('layouts.auth')

@section('content')
    <div class="container-scroller login">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper mt-0 d-flex align-items-center auth register-bg-1 theme-one">
                <div class="row w-100">
                    <div class="col-md-6 col-lg-4 mx-auto">
                        <div class="auto-form-wrapper text-center">
                            <a class="navbar-brand" href="#"><img src="{{ asset('website/build/public/img/logo-dark.png') }}" alt="Debugtool - The Only Agency Management Platform Dedicated To Website Design & Development"></a>

                            <div class="mb-4 text-sm text-gray-600">
                                {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
                            </div>

                            @if (session('status') == 'verification-link-sent')
                                <div class="mb-4 font-medium text-sm text-green-600">
                                    {{ __('A new verification link has been sent to the email address you provided during registration.') }}
                                </div>
                            @endif

                            <form method="POST" action="{{ route('verification.send') }}">
                                @csrf
                                <div class="form-group">
                                    <button class="btn btn-primary submit-btn btn-block" type="submit">{{ __('Resend Verification Email') }}</button>
                                </div>
                            </form>

                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <button type="submit" class="btn btn-link text-block text-center text-dark underline text-sm text-gray-600 hover:text-gray-900">
                                    {{ __('Log Out') }}
                                </button>
                            </form>
                        </div>
                        <ul class="auth-footer">
                            <li>
                                <a href="#">{{__('Terms of use')}}</a>
                            </li>
                            <li>
                                <a href="#">{{__('Privacy Policy')}}</a>
                            </li>
                            <li>
                                <a href="#">{{__('Cookies Policy')}}</a>
                            </li>
                        </ul>
                        <p class="footer-text text-center">{{trans('view.All rights reserved.')}} Copyright ©{{ date('Y') }}</p>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
@endsection
