<!-- partial:partials/_footer.html -->
<footer class="footer">
    <div class="container-fluid clearfix">
        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © DebugTool <?php echo date('Y'); ?></span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> {{__('Organise your workflow with DebugTool.com.')}}</span>
    </div>
</footer>
<!-- partial -->
