
<nav class="sidebar nobackground sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            @livewire('sidebar')
        </li>
        <li class="nav-item pt-2">
            <a class="nav-link" href="#">
                @if(@$where == 'dashboard') <span class="before"></span> @endif
                <span class="icon dashboard"></span>
                <span class="menu-title">{{__('Dashboard')}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                @if(@$where == 'projects') <span class="before"></span> @endif
                <span class="icon create"></span>
                <span class="menu-title">{{__('Projects')}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                @if(@$where == 'center') <span class="before"></span> @endif
                <span class="icon team"></span>
                <span class="menu-title">{{__('Center')}}</span>
            </a>
        </li>
        <li class="nav-item @if($where == "tasks") active @endif " >
            <a class="nav-link" href="#" @if($where == "tasks") aria-expanded="true" @endif >
                <span class="icon project"></span>
                <span class="menu-title">{{__('Boards')}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span class="icon time"></span>
                <span class="menu-title">{{__('Time tracking')}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span class="icon invoice"></span>
                <span class="menu-title">{{__('Invoices')}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                @if(@$where == 'subscription') <span class="before"></span> @endif
                <span class="icon payment"></span>
                <span class="menu-title">{{__('Debug plans')}}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span class="icon help"></span>
                <span class="menu-title">{{__('Help')}}</span>
            </a>
        </li>
    </ul>
</nav>
