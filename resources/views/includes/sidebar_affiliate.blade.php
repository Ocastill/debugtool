
<nav class="sidebar nobackground sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            @livewire('sidebar')
        </li>
        <li class="nav-item pt-2 @if(this_route() == 'affiliate_dashboard') active @endif">
            <a class="nav-link" href="{{ route('affiliate_dashboard') }}">
                @if(this_route() == 'affiliate_dashboard') <span class="before"></span> @endif
                <span class="icon dashboard"></span>
                <span class="menu-title">{{__('Dashboard')}}</span>
            </a>
        </li>
        <li class="nav-item @if(this_route() == 'affiliate_stats') active @endif">
            <a class="nav-link" href="{{ route('affiliate_stats') }}">
                @if(this_route() == 'stats') <span class="before"></span> @endif
                <span class="icon stats"></span>
                <span class="menu-title">{{__('Stats')}}</span>
            </a>
        </li>
        <li class="nav-item @if(this_route() == 'affiliate_mailing') active @endif">
            <a class="nav-link" href="{{ route('affiliate_mailing') }}">
                @if(this_route() == 'affiliate_mailing') <span class="before"></span> @endif
                <span class="icon mailing"></span>
                <span class="menu-title">{{__('Mailing')}}</span>
            </a>
        </li>
        <li class="nav-item @if(this_route() == 'affiliate_promotion') active @endif">
            <a class="nav-link" href="{{ route('affiliate_promotion') }}">
                @if(this_route() == 'affiliate_promotion') <span class="before"></span> @endif
                <span class="icon promotion"></span>
                <span class="menu-title">{{__('Promotion')}}</span>
            </a>
        </li>
        <li class="nav-item @if(this_route() == 'affiliate_whitelabel') active @endif">
            <a class="nav-link" href="{{ route('affiliate_whitelabel') }}">
                @if(this_route() == 'affiliate_whitelabel') <span class="before"></span> @endif
                <span class="icon whitelabel"></span>
                <span class="menu-title">{{__('White label')}}</span>
            </a>
        </li>
        <li class="nav-item @if(this_route() == 'affiliate_payments') active @endif">
            <a class="nav-link" href="{{ route('affiliate_payments') }}">
                @if(this_route() == 'affiliate_payments') <span class="before"></span> @endif
                <span class="icon payments"></span>
                <span class="menu-title">{{__('Payments')}}</span>
            </a>
        </li>
    </ul>
</nav>
