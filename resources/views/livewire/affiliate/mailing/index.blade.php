@section('title')
    {{ $title }}
@endsection

<div>
    <div class="content-wrapper center">
        @if(!empty($title))
            <div class="row page-title-header">
                <div class="col-12">
                    <div class="page-header">
                        <h4 class="page-title">{{ $title }}</h4>
                    </div>
                </div>
            </div>
        @endif

        <div class="topbar">
            <div class="left bold">
                @if($table == 'templates')
                    <a data-toggle="modal" wire:click="modalAddTemplate"><span class="add round btn-small reverse"><i class="fas fa-plus"></i> {{__('Create email template')}}</span></a>
                @endif
                @if($table == 'clients')
                    <a href="#"><span class="add round btn-small reverse"><i class="fas fa-plus"></i> {{__('Add client')}}</span></a>
                @endif


                {{--<a data-toggle="modal" wire:click="modalTemplate"> <span class="add round btn-small reverse"><i class="fas fa-plus"></i> {{__('Create email template')}}</span></a>--}}
            </div>
        </div>

        <div class="cont ">
            <div class="card">
                <div class="card-body">
                    <div class="tab">
                        <a @if($table == 'templates') class="active" @endif wire:click="table('templates')">{{__('Templates') }}</a>
                        <a @if($table == 'clients') class="active" @endif wire:click="table('clients')">{{__('Clients')}}</a>
                        <a @if($table == 'batches') class="active" @endif wire:click="table('batches')">{{__('Batches')}}</a>
                    </div>
                    <div class="table-responsive">

                        @if($table == 'templates')
                            <table class="table table-hover table-templates">
                                <thead>
                                    <tr>
                                        <th>{{__('Name')}} <a wire:click="sort('templates', 'name')"><i class="fas fa-sort"></i></a></th>
                                        <th>{{__('Description')}} <a wire:click="sort('templates', 'description')"><i class="fas fa-sort"></i></a></th>
                                        <th>{{__('Actions')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($templates->isNotEmpty())
                                        @foreach($templates as $template)
                                            <tr>
                                                <td>{{ $template->name }}</td>
                                                <td>{{ $template->description }}</td>
                                                <td>
                                                    @if($clients->isNotEmpty())<a class="blues" wire:click="modalMailTemplate({{$template->id}})" alt="{{__('Send email')}}" title="{{__('Send email')}}"><span class="block"><i class="far fa-paper-plane"></i></span></a>@endif
                                                    <a class="blues" wire:click="modalEditTemplate({{$template->id}})" alt="{{__('Edit template')}}" title="{{__('Edit template')}}"><span class="block"><i class="far fa-edit"></i></span></a>
                                                    <a class="reds" wire:click="confirm({{$template->id}})" alt="{{__('Delete template')}}" title="{{__('Delete template')}}"><span class="block"><i class="far fa-trash-alt"></i></span></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3">
                                                <div class="text-center text-muted mt-5 mb-5"><em>{{__('You don\'t have templates added yet')}}</em></div>
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        @endif

                        @if($table == 'clients')
                            <table class="table table-hover table-clients">
                                <thead>
                                <tr>
                                    <th>{{__('Name')}} <a wire:click="sort('clients', 'name')"><i class="fas fa-sort"></i></a></th>
                                    <th>{{__('Email')}} <a wire:click="sort('clients', 'email')"><i class="fas fa-sort"></i></a></th>
                                    <th>{{__('Role')}}</th>
                                    <th>{{__('Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($clients->isNotEmpty())
                                    @foreach($clients as $client)
                                        <tr>
                                            <td>{{ $client->name }}</td>
                                            <td>{{ $client->email }}</td>
                                            <td class="text-capitalize">{{ $client->role }}</td>
                                            <td>
                                                <a class="blues" wire:click="edit($template->id)" alt="{{__('Send email')}}" title="{{__('Send email')}}"><span class="block"><i class="far fa-envelope"></i></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">
                                            <div class="text-center text-muted mt-5 mb-5"><em>{{__('You still don\'t have registered clients')}}</em></div>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        @endif

                    </div>
                </div>
            </div>
        </div>

        <div wire:loading wire:target="showMembers, showEditTeam, showClients, addClient, sortName, sortEmail, addTemplate, modalTemplate">
            <img src="{{ asset('img/loading-gif.gif') }}" class="loader" />
        </div>

        <div wire:ignore.self class="modal fade" id="addTemplate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">{{__('Create email template')}}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="inside-form">
                                @if(session()->has('successTemplate'))
                                    <div class="alert alert-success mb-3 mt-4 alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        {{ session('successTemplate') }}
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="template_name" class="form-label">{{__('Name')}}</label>
                                    <input wire:model="template_name" type="text" class="form-control" :errors="$errors" autocomplete="off" />
                                    @error('template_name') <span class="error">{{ $message }}</span> @enderror
                                </div>
                                <div class="form-group">
                                    <label for="template_description" class="form-label">{{__('Description')}}</label>
                                    <input wire:model="template_description" type="text" class="form-control" :errors="$errors" autocomplete="off" />
                                    @error('template_description') <span class="error">{{ $message }}</span> @enderror
                                </div>
                                <div class="form-group">
                                    <label for="notes" class="form-label">{{__('Email')}}</label>
                                    <div class="mb-3" wire:ignore>
                                        <div x-data
                                             x-ref="quillEditor"
                                             x-init="
                                                 quill = new Quill($refs.quillEditor, {theme: 'snow'});
                                                 quill.on('text-change', function () {
                                                   $dispatch('input', quill.root.innerHTML);
                                                   @this.set('content', quill.root.innerHTML)
                                                 });
                                            "
                                             wire:model.lazy="template_content"
                                        >
                                            {!! $content !!}
                                        </div>
                                    </div>
                                    @error('content')<span class="error">{{ $message }}</span>@enderror
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <div wire:loading wire:target="addTemplate">
                                <img src="{{ asset('img/loading-gif.gif') }}" class="loader" />
                            </div>
                            <div wire:loading.remove wire:target="addTemplate">
                                <button type="button" wire:click="addTemplate" class="btn btn-primary">{{__('Save template')}}</button>
                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Cancel')}}</button>
                        </div>
                    </div>
                </div>
            </div>

        <div wire:ignore.self class="modal fade" id="editTemplate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__('Create email template')}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="inside-form">
                            @if(session()->has('successTemplate'))
                                <div class="alert alert-success mb-3 mt-4 alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ session('successTemplate') }}
                                </div>
                            @endif
                            <input type="hidden" wire:model="template_id" />
                            <div class="form-group">
                                <label for="template_name" class="form-label">{{__('Name')}}</label>
                                <input wire:model="template_name" type="text" class="form-control" :errors="$errors" autocomplete="off" />
                                @error('template_name') <span class="error">{{ $message }}</span> @enderror
                            </div>
                            <div class="form-group">
                                <label for="template_description" class="form-label">{{__('Description')}}</label>
                                <input wire:model="template_description" type="text" class="form-control" :errors="$errors" autocomplete="off" />
                                @error('template_description') <span class="error">{{ $message }}</span> @enderror
                            </div>
                            <div class="form-group">
                                <label for="notes" class="form-label">{{__('Email')}}</label>
                                <div class="mb-3" wire:ignore>
                                    <div x-data
                                         x-ref="quillEditor"
                                         x-init="
                                             quill_data = '{{ $template_content }}';
                                             quill_edit = new Quill($refs.quillEditor, {theme: 'snow'});
                                             quill_edit.on('text-change', function () {
                                               $dispatch('input', quill_edit.root.innerHTML);
                                               @this.set('content', quill_edit.root.innerHTML)
                                             });
                                        "
                                         wire:model.lazy="template_content"
                                    >
                                        {!! $content !!}
                                    </div>
                                </div>
                                @error('content')<span class="error">{{ $message }}</span>@enderror
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div wire:loading wire:target="editTemplate">
                            <img src="{{ asset('img/loading-gif.gif') }}" class="loader" />
                        </div>
                        <div wire:loading.remove wire:target="editTemplate">
                            <button type="button" wire:click="editTemplate" class="btn btn-primary">{{__('Update template')}}</button>
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Cancel')}}</button>
                    </div>
                </div>
            </div>
        </div>

        <div wire:ignore.self class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">{{__('Confirm delete')}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <p>{{__('Are you sure want to delete?')}}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="button" wire:click.prevent="delete" class="btn btn-danger close-modal" data-dismiss="modal">
                            {{__('Yes, Delete')}}</button>
                    </div>
                </div>
            </div>
        </div>

        <div wire:ignore.self class="modal fade" id="mailTemplate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">{{__('Create email template')}}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="inside-form">
                                @if(session()->has('successMail'))
                                    <div class="alert alert-success mb-3 mt-4 alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        {{ session('successMail') }}
                                    </div>
                                @endif
                                <input type="hidden" wire:model="mail_id" />
                                <div class="form-group">
                                    <label for="recipients" class="form-label">{{__('Clients')}}</label>
                                    <input wire:model="recipients" type="text" class="form-control" :errors="$errors" autocomplete="off" />
                                    @error('recipients') <span class="error">{{ $message }}</span> @enderror
                                </div>
                                <div class="form-group">
                                    <label for="subject" class="form-label">{{__('Subject')}}</label>
                                    <input wire:model="subject" type="text" class="form-control" :errors="$errors" autocomplete="off" />
                                    @error('subject') <span class="error">{{ $message }}</span> @enderror
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="batch" wire:model="batch">
                                    <label class="form-check-label" for="batch">
                                        {{__('Email batches')}}
                                    </label>
                                </div>
                                @if($batch)
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="batch_size" class="form-label">{{__('Batch size')}} ({{__('emails')}})</label>
                                            <input wire:model="batch_size" type="number" class="form-control" :errors="$errors" autocomplete="off" />
                                            @error('batch_size') <span class="error">{{ $message }}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="batch_interval" class="form-label">{{__('Batch interval')}} ({{__('hours')}})</label>
                                            <input wire:model="batch_interval" type="number" class="form-control" :errors="$errors" autocomplete="off" />
                                            @error('batch_interval') <span class="error">{{ $message }}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div wire:loading wire:target="mailTemplate">
                                <img src="{{ asset('img/loading-gif.gif') }}" class="loader" />
                            </div>
                            <div wire:loading.remove wire:target="mailTemplate">
                                <button type="button" wire:click="mailTemplate" class="btn btn-primary">{{__('Send email')}}</button>
                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Cancel')}}</button>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>


@section('javascript')
    <script>
        window.addEventListener('alert', event => {
            alert(event.detail.message);
        });

        window.addEventListener('modal', event => {
            if(event.detail) {
                $(event.detail).modal('show');
            }
        });
    </script>
@endsection

@push('scripts')
    <script>
        window.addEventListener('showAddTemplate', event => {
            $('#addTemplate').modal('show');
        });

        window.addEventListener('hideAddTemplate', event => {
            $('#addTemplate').modal('hide');
        });

        window.addEventListener('showEditTemplate', event => {
            quill_edit.container.firstChild.innerHTML = event.detail.editor;
            $('#editTemplate').modal('show');
        });

        window.addEventListener('hideEditTemplate', event => {
            $('#editTemplate').modal('hide');
        });

        window.addEventListener('confirmDelete', event => {
            $('#confirmModal').modal('show');
        });

        window.addEventListener('showMailTemplate', event => {
            $('#mailTemplate').modal('show');
        });

        document.addEventListener('livewire:load', function () {

        });
    </script>
    @endpush
