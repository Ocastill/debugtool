@section('title')
    {{ $title }}
@endsection

<div>
    <div class="content-wrapper">
        @if(!empty($title))
            <div class="row page-title-header">
                <div class="col-12">
                    <div class="page-header">
                        <h4 class="page-title">{{ $title }}</h4>
                    </div>
                </div>
            </div>
        @endif



    </div>
</div>
