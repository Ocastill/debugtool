<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- If you delete this tag, the sky will fall on your head -->
    <meta name="viewport" content="width=device-width" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;500;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;500;600;700;900&display=swap" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Debugtool | {{trans('view.Uw inschrijving')}}</title>

    <style type="text/css">
        * {
            margin:0;
            padding:0;
        }
        * { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

        h1, h2 , h3 , h4 , h5 , h6 {
            font-weight: bold;
            color: #4382cd;
        }

        h1, h2 , h3{
            font-size: 32px;
        }

        body {
            font-family: "Nunito",sans-serif;
            color: #444444;
        }

        p{
            font-size: 16px;
            line-height: 25px;
            color: #444444;
        }

        img {
            max-width: 100%;
        }
        .collapse {
            margin:0;
            padding:0;
        }
        body {
            -webkit-font-smoothing:antialiased;
            -webkit-text-size-adjust:none;
            width: 100%!important;
            height: 100%;
        }

        a { color: #2BA6CB;}

        .btn {
            text-decoration:none;
            color: #FFF;
            background-color: #666;
            padding:10px 16px;
            font-weight:bold;
            margin-right:10px;
            text-align:center;
            cursor:pointer;
            display: inline-block;
        }

        p.callout {
            padding:15px;
            background-color:#ECF8FF;
            margin-bottom: 15px;
        }

        .callout a {
            font-weight:bold;
            color: #444444;
        }

        .social .soc-btn {
            padding: 3px 7px;
            font-size:12px;
            margin-bottom:10px;
            text-decoration:none;
            color: #FFF;font-weight:bold;
            display:block;
            text-align:center;
        }

        a.fb { background-color: #3B5998!important; }
        a.tw { background-color: #1daced!important; }
        a.gp { background-color: #DB4A39!important; }
        a.ms { background-color: #000!important; }

        .sidebar .soc-btn {
            display:block;
            width:100%;
        }

        table.head-wrap { width: 100%;}

        .header.container table td.logo { padding: 15px; }
        .header.container table td.label { padding: 15px; padding-left:0px;}


        table.body-wrap { width: 100%;}


        table.footer-wrap { width: 100%;	clear:both!important;
        }
        .footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
        .footer-wrap .container td.content p {
            font-size:10px;
            font-weight: bold;

        }

        .collapse { margin:0!important;}

        p, ul {
            margin-bottom: 10px;
            font-weight: normal;
            font-size:14px;
            line-height:1.6;
        }
        p.lead { font-size:17px; }
        p.last { margin-bottom:0px;}

        ul li {
            margin-left:5px;
            list-style-position: inside;
        }

        ul.sidebar {
            background:#ebebeb;
            display:block;
            list-style-type: none;
        }
        ul.sidebar li { display: block; margin:0;}
        ul.sidebar li a {
            text-decoration:none;
            color: #666;
            padding:10px 16px;
            margin-right:10px;
            cursor:pointer;
            border-bottom: 1px solid #777777;
            border-top: 1px solid #FFFFFF;
            display:block;
            margin:0;
        }
        ul.sidebar li a.last { border-bottom-width:0px;}
        ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}



        .container {
            display:block!important;
            max-width:600px!important;
            margin:0 auto!important;
            clear:both!important;
        }

        .content {
            padding:15px;
            max-width:600px;
            margin:0 auto;
            display:block;
        }

        .content table { width: 100%; }


        .column {
            width: 300px;
            float:left;
        }
        .column tr td { padding: 15px; }
        .column-wrap {
            padding:0!important;
            margin:0 auto;
            max-width:600px!important;
        }
        .column table { width:100%;}
        .social .column {
            width: 280px;
            min-width: 279px;
            float:left;
        }

        .content.mail{
            text-align:center;
        }

        strong.intro{
            margin-bottom: 40px;
            font-size: 20px;
            display:block;
        }

        h3.graybar{
            padding-top: 40px;
            border-top: 4px solid #ebebeb;
            padding-bottom: 20px;
            font-size: 36px;
        }

        .footer-wrap{
            background-color: #f0f0f0;
            padding:40px;
        }

        .clear { display: block; clear: both; }

        .btn.btn-success{
            width: 90%;
            text-transform: uppercase;
            background-color: #4684ca !important;
            font-size: 18px;
            border-radius: 4px;
        }

        .auth-footer {
            list-style-type: none;
            padding-left: 0;
            margin-top: 20px;
            margin-bottom: 10px;
            display: flex;
            justify-content: center;
        }

        .footer-wrap{
            font-size: 18px;
        }

        .footer-wrap p{
            color: #848484;
        }

        .auth-footer li {
            color: #848484;
            margin-bottom:40px;
        }
        .auth-footer li {
            color: #848484;
        }
        .auth-footer li a{
            color: #848484 !important;
            text-decoration: none;
        }
        .auth-footer li {
            margin-right: 10px;
            line-height: 1;
            padding-right: 10px;
            border-right: 1px solid #848484;
        }
        .auth-footer li:last-child{
            border-right: 0px solid #848484;
        }

        p, ul {
            margin-bottom: 10px;
            font-weight: normal;
            font-size: 16px;
            line-height: 1.4;
        }

        .footer-wrap{
            text-align:center;
        }

        .auth-footer li a {
            color: #848484 !important;
            text-decoration: none;
            font-size: 12px;
        }

        .content {
            padding-left: 5px;
        }

        .auth-footer li {
            margin-right: -1px;
            line-height: 1;
            padding-right: 6px;
            border-right: 1px solid #848484;
        }

        .auth-footer {
            list-style-type: none;
            padding-left: 0;
            margin-top: 5px;
            margin-bottom: 0px;
            display: flex;
            justify-content: center;
        }

        .footer-wrap {
            background-color: #f0f0f0;
            padding: 0px;
        }
    </style>

</head>

<body bgcolor="#f9f9f9">

<!-- HEADER -->
<table class="head-wrap" bgcolor="#3cd4e1" style="background:#3cd4e1;background:url('<?php echo config('app.url'); ?>/img/headermail.jpg');background-size:cover;">
    <tr>
        <td></td>
        <td class="header container">

            <div class="content">
                <table class="headsup"  >
                    <tr>
                        <td style="text-align:center;"><img src="<?php echo config('app.url'); ?>/img/debugmail.png" style="max-width: 300px;"/></td>
                    </tr>
                </table>
            </div>

        </td>
        <td></td>
    </tr>
</table><!-- /HEADER -->


<!-- BODY -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" >

            <div class="content mail">
                <table>
                    <tr>
                        <td>
                            <h2>{{__('There\'s is better way to work')}}</h2>
                            <strong class="intro">{{__('Your team is excited you\'ve joined them in DebugTool.')}}</strong>
                            <img src="<?php echo config('app.url'); ?>/img/hoera.png" alt="" style="max-width:65%;margin-bottom:10px;" />
                            @if (!@custom)
                                <h3 class="graybar">{{trans('view.Hallo')}} {{$name}},</h3>
                            @endif
                            <p class="lead">{!!$contents!!}</p>
                            @if(!empty($link))
                                <p><br/></p>
                                <p><a href="{{$link}}" class="btn btn-success" style="background-color:#3cd4e1;color:#fff !important;">{{$button}}</a></p>
                                <p><br/></p>
                            @endif
                            @if(!empty($extra))
                                <p>{!!$extra!!}</p>
                            @endif
                        </td>
                    </tr>
                </table>
            </div>

        </td>
        <td></td>
    </tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap">
    <tr>
        <td></td>
        <td class="container">
            <!-- content -->
            <div class="content">
                <table>
                    <tr>
                        <td align="center">
                            <p>Copyright @<?PHP echo date('Y'); ?> DebugTool </p>
                            <ul class="auth-footer">
                                <li>
                                    <a href="#" style="color: #848484;">{{__('Terms of use')}}</a>
                                </li>
                                <li>
                                    <a href="#" style="color: #848484;">{{__('Privacy Policy')}}</a>
                                </li>
                                <li>
                                    <a href="#" style="color: #848484;">{{__('Cookies Policy')}}</a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>

                <p style="font-size:12px;"> {{__('No more mails?')}} <a href="#" style="color: #848484;">{{__('Unsubscribe')}}</a> </p>
            </div><!-- /content -->

        </td>
        <td></td>
    </tr>
</table><!-- /FOOTER -->

</body>
</html>
