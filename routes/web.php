<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () { return view('dashboard'); })->name('dashboard');

# Verify email
Route::get('email/verify', function() { return view('auth.verify-email'); })->middleware('auth')->name('verification.notice');
Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) { $request->fulfill(); return redirect('/dashboard'); })->middleware(['auth', 'signed'])->name('verification.verify');
Route::post('/email/verification-notification', function (Request $request) { $request->user()->sendEmailVerificationNotification(); return back()->with('status', 'verification-link-sent'); })->middleware(['auth', 'throttle:6,1'])->name('verification.send');

#Affiliate
Route::group(['middleware' => ['auth:sanctum', 'verified'], 'namespace' => '\App\Http\Livewire\Affiliate'], function ($router) {
    Route::get('affiliate/dashboard', Dashboard::class)->name('affiliate_dashboard');
    Route::get('affiliate/stats', Stats::class)->name('affiliate_stats');
    Route::get('affiliate/mailing', Mailing\Index::class)->name('affiliate_mailing');
    Route::get('affiliate/promotion', Promotion\Index::class)->name('affiliate_promotion');
    Route::get('affiliate/whitelabel', Whitelabel\Index::class)->name('affiliate_whitelabel');
    Route::get('affiliate/payments', Payments\Index::class)->name('affiliate_payments');
});

#Hits
Route::get('stats/{affiliate}/hit/{client}', [App\Http\Controllers\Affiliates\Statistics::class, 'hits']);
